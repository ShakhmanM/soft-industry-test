<?php

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(0, 15) as $index) {
            factory(Project::class, 1)->create([
                'name' => 'Проект ' . $index,
            ]);
        }
    }
}
