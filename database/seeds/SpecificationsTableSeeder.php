<?php

use App\Models\Specification;
use Illuminate\Database\Seeder;

class SpecificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = \Carbon\Carbon::now()->toDateTimeString();

        $data = [
            [
                'name' => 'Коммуникабельность',
                'title' => 'communication',
            ],
            [
                'name' => 'Инженерный навык',
                'title' => 'engineering',
            ],
            [
                'name' => 'Тайм менеджмент',
                'title' => 'time_management',
            ],
            [
                'name' => 'Знание языков',
                'title' => 'languages_skills',
            ],
        ];

        foreach ($data as &$val) {
            $val['created_at'] = $timestamp;
            $val['updated_at'] = $timestamp;
        }

        Specification::insert($data);
    }
}
