<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Models\Employee;

class EmployeesGetAll extends Controller
{
    public function __invoke()
    {
        $result = Employee::where('active', true)->orderBy('id', 'desc')->get();

        return response($result, self::HTTP_OK);
    }
}
