<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeStoreRequest as Request;
use App\Models\Employee;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class EmployeesStore extends Controller
{
    private $request;
    private $employee;
    private $imagePath;

    public function __invoke(Request $request)
    {
        $this->request = $request;
        $this->store();

        return response([
            'message' => 'Сотрудник успешно создан',
        ], self::HTTP_CREATED);
    }

    private function store()
    {
        DB::beginTransaction();

        try {
            $this->saveImage();
            $this->storeEmployee();
            $this->storeEmployeesSpecifications();
            $this->storeEmployeesProjects();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();
    }

    private function saveImage()
    {
        if ($this->request->has('image')) {
            $this->imagePath = $this->request->file('image')->store('avatars');
            $this->imagePath = explode('/', $this->imagePath)[1];
        }
    }

    private function storeEmployee(): void
    {
        $data = [
            'full_name' => $this->request['fullName'],
        ];

        if ($this->imagePath) {
            $data['avatar'] = $this->imagePath;
        }

        $this->employee = Employee::create($data);
    }

    private function storeEmployeesSpecifications(): void
    {
        $specifications = (array) json_decode($this->request['specifications']);

        if (!empty($specifications)) {
            $data = [];
            foreach ($specifications as $id => $specification) {
                $data[] = [
                    'specification_id' => $id,
                    'value' => (string) $specification,
                ];
            }
            $this->employee->specifications()->attach($data);
        }
    }

    private function storeEmployeesProjects(): void
    {
        $projectsArr = explode(',', $this->request['projects']);

        if (!empty($projectsArr)) {
            $data = [];
            $projects = Project::whereIn('name', $projectsArr)->pluck('id');
            foreach ($projects as $projectId) {
                $data[] = [
                    'project_id' => $projectId,
                ];
            }
            $this->employee->projects()->attach($data);
        }
    }
}
