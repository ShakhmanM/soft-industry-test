<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Models\Project;

class ProjectsGetAll extends Controller
{
    public function __invoke()
    {
        $result = Project::where('active', true)->pluck('name', 'id');

        return response($result, self::HTTP_OK);
    }
}
