<?php

namespace App\Http\Controllers\Specifications;

use App\Http\Controllers\Controller;
use App\Models\Specification;

class SpecificationsGetAll extends Controller
{
    public function __invoke()
    {
        $result = Specification::where('active', true)->pluck('name', 'id');

        return response($result, self::HTTP_OK);
    }
}
