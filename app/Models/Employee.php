<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $fillable = [
        'full_name',
        'avatar',
    ];

    protected $appends = [
        'projects_count',
        'specifications',
        'specifications_avg',
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'active',
    ];

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'employees_to_projects');
    }

    public function specifications()
    {
        return $this->belongsToMany('App\Models\Specification', 'employees_to_specifications')->withPivot('value');
    }

    public function getProjectsCountAttribute()
    {
        return $this->projects()->count();
    }

    public function getAvatarAttribute($value)
    {
        return '/uploads/avatars/' . $value;
    }

    public function getSpecificationsAttribute()
    {
        $result = [];
        $specifications = $this->specifications()->get();

        if (!empty($specifications)) {
            $specifications->map(function ($item) use (&$result) {
                return $result[$item['title']] = $item['pivot']['value'];
            });
        }

        return $result;
    }

    public function getSpecificationsAvgAttribute()
    {
        $result = 0;
        $specifications = $this->specifications()->get();

        if (!empty($specifications)) {
            $count = $specifications->count();
            $specifications->map(function ($item) use (&$result) {
                return $result += $item['pivot']['value'];
            });
            $result = $count ? $result / $count : $result;
        }

        return $result;
    }
}
