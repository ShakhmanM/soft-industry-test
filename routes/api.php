<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'projects'], function () {
    Route::get('/', 'Projects\ProjectsGetAll');
});

Route::group(['prefix' => 'specifications'], function () {
    Route::get('/', 'Specifications\SpecificationsGetAll');
});

Route::group(['prefix' => 'employees'], function () {
    Route::post('/', 'Employees\EmployeesStore');
    Route::get('/', 'Employees\EmployeesGetAll');
});
