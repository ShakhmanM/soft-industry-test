import axios from 'axios';

const url = 'api/employees/';

function getAll() {
    return new Promise((resolve, reject) => {
        return axios.get(`${url}`, )
            .then(response => {
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
}

function store(data) {
    return new Promise((resolve, reject) => {
        return axios.post(`${url}`, data, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
            .then(response => {
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
}

export {
    getAll,
    store,
}
