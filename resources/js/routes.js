import Employees from './pages/employees/Index';
import EmployeesCreate from './pages/employees/create/Index';

export const routes = {
    routes: [{
            path: '/employees',
            component: Employees,
            name: 'employees',
        },
        {
            path: '/employees/create',
            component: EmployeesCreate,
            name: 'employees.create',
        },
        {
            path: '/',
            redirect: 'employees',
        },
    ]
}
